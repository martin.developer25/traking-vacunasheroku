from django.shortcuts import render
from rest_framework import generics, serializers, filters, generics, viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import action
from .models import Data, Data_Loger, Container, Location, Heredity
from .serializers import DataSerializer, DataLogerSerializer, ContainerSerializer, LocationSerializer, HereditySerializer #,UserSerializer, RegisterSerializer, LoginSerializer
#from knox.models import AuthToken
"""
# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# lOGIN API
class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# GET USER API
class UserAPI(generics.RetrieveAPIView):
    permissions_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

"""


# Create your views here.
class dataViewSet(viewsets.ViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer

    """permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = DataSerializer

    def get_queryset(self):
        return Data.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)"""
    @action(detail=False, methods=['GET'])
    def get(self, request, format=None):
         queryset = Data.objects.all()
         serializer = DataSerializer(queryset, many=True)
         return Response(serializer.data, status=status.HTTP_200_OK)


    @action(detail=False, methods=['POST'])
    def custom_create(self, request, *args, **kwargs):
    
        instance = Data
        instance2 = Location
        datos = request.data
        length = len(datos["Datos"]["Fecha"])
        for i in range(0,length, 1):
            if i < length:
                if i == 0:
                    data_loger= Data_Loger.objects.get(pk=datos["id_data_logger"])
                    #print(id_data_loger)
                    #Llenado de la tabla LOCATION
                    instance2.objects.create(countryName= datos["Rastreo"]["countryName"], 
                    countryCode= datos["Rastreo"]["countryCode"], 
                    adminArea= datos["Rastreo"]["adminArea"], 
                    locality= datos["Rastreo"]["locality"], 
                    postalCode= datos["Rastreo"]["postalCode"], 
                    latitud= datos["Rastreo"]["latitude"], 
                    longitud= datos["Rastreo"]["longitude"],
                    id_data_loger= data_loger
                    )
                

                #Llenado de la tabla Data
                instance.objects.create(id_data_loger= data_loger, temperature= datos["Datos"]["Temperatura"][i], 
                date= datos["Datos"]["Fecha"][i], hours=  datos["Datos"]["Hora"][i], alarm= datos["Datos"]["Alarma"], 
                date_transmition= datos["Conversion"]["Fecha"], hours_transmition= datos["Conversion"]["Hora"])       
                 
        return Response('Query Ready',status=status.HTTP_200_OK)


#Proceso para hacer la herencia de dataloggers
class HeredityViewSet(viewsets.ViewSet):
    queryset = Heredity.objects.all()
    serializer_class = HereditySerializer

    @action(detail=False, methods=['GET'])
    def get(self, request, format=None):
        queryset = Heredity.objects.all()
        serializer = HereditySerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    #LISTA TODOS LOS DATALOGGER HIJOS
    @action(detail=False, methods=['GET'])
    def get_son(self, request, format=None):
        queryset = Heredity.objects.all()

        father = request.query_params.get('datalogger_father', None)

        if father is not None:
            queryset = queryset.filter(datalogger_father= father)

        serializer = HereditySerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    #LISTA TODOS LOS DATALOGGER PADRE
    @action(detail=False, methods=['GET'])
    def get_only_father(self, request, format=None):
        queryset = Heredity.objects.all()

        father = request.query_params.get('datalogger_son', None)

        if father is not None:
            queryset = queryset.filter(datalogger_son= father)

        serializer = HereditySerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    #CARGAR LOS DATOS DE LA HERENCIAS
    @action(detail=False, methods=['POST'])
    def custom_create(self, request, *args, **kwargs):
        datos = request.data
        Heredity.objects.create(datalogger_father=datos['datalogger_father'], datalogger_son=datos['datalogger_son'])
        return Response(datos,status=status.HTTP_200_OK)


class LocationViewSet(viewsets.ViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    @action(detail=False, methods=['GET'])
    def get_location_datalogger (self, request, format=None):
        #queryset = Location.objects.all()
        
        queryset = Location.objects.select_related('id_data_loger')

        serializer_class = LocationSerializer(queryset, many=True)
        return Response(serializer_class.data, status=status.HTTP_200_OK)


class DataLoggerViewSet(viewsets.ViewSet):
    queryset = Data_Loger.objects.all()
    serializer_class = DataLogerSerializer

    #CARGAR LOS DATALOGGERS
    @action(detail=False, methods=['POST'])
    def custom_create(self, request, *args, **kwargs):
        datos = request.data
        
        print(datos['idLote'])
        Data_Loger.objects.create(id_container=datos['idLote'], id_data_loger=datos["DataLogger"]["hijo"], id_status=True,)
        
        data_loger= Data_Loger.objects.get(pk=datos["DataLogger"]["hijo"])

        Location.objects.create(countryName= datos["Rastreo"]["countryName"], 
                    countryCode= datos["Rastreo"]["countryCode"], 
                    adminArea= datos["Rastreo"]["adminArea"], 
                    locality= datos["Rastreo"]["locality"], 
                    postalCode= datos["Rastreo"]["postalCode"], 
                    latitud= datos["Rastreo"]["latitude"], 
                    longitud= datos["Rastreo"]["longitude"],
                    id_data_loger= data_loger,
                    )

        return Response('Query Ready',status=status.HTTP_200_OK)

    #MOSTRAR TODOS LOS DATALOGGERS
    @action(detail=False, methods=['GET'])
    def get_datalogger (self, request, format=None):
        #queryset = Location.objects.all()
        
        queryset = Data_Loger.objects.all()
        serializer_class = DataLogerSerializer(queryset, many=True)
        return Response(serializer_class.data, status=status.HTTP_200_OK)

    #MOSTRAR TODOS LOS DATALOGGERS
    @action(detail=False, methods=['GET'])
    def get_only_datalogger(self, request, format=None):
        queryset = Data_Loger.objects.all()

        dlogger = request.query_params.get('datalogger', None)
        print(dlogger)

        if dlogger is not None:
            queryset = queryset.filter(id_data_loger= dlogger)

        serializer = DataLogerSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    #MOSTRAR TODOS LOS DATALOGGERS
    @action(detail=False, methods=['GET'])
    def get_datalogger_status(self, request, format=None):
        queryset = Data_Loger.objects.all()

        dstatus = request.query_params.get('status', None)
        print(dstatus)

        if dstatus is not None:
            queryset = queryset.filter(id_status= dstatus)

        serializer = DataLogerSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)



class ListContainer(viewsets.ViewSet):
    queryset = Container.objects.all()
    serializer_class = ContainerSerializer

