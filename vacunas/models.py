from django.db import models
#from django.contrib.auth.models import User
# Create your models here.


class Data_Loger(models.Model):
    id_container = models.CharField(max_length=200)
    id_data_loger = models.CharField(max_length=200, primary_key=True, unique=True)
    id_status = models.BooleanField(null=True)

    def __str__(self):
        return self.id_data_loger


class Data(models.Model):
    id_data_loger = models.ForeignKey('Data_Loger', on_delete=models.CASCADE, null=True)
    temperature = models.FloatField()
    date = models.DateField(default=False)
    hours = models.TimeField(auto_now= False)
    alarm = models.BooleanField(default=False)
    date_transmition = models.DateField(auto_now= False)
    hours_transmition = models.TimeField(auto_now= False)
    #owner = models.ForeignKey(
      #  User, related_name="leads", on_delete=models.CASCADE, null=True)
    created_on = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now= True)

    def __str__(self):
        return "Datalogger: {}, Temperature: {}, Fecha y Hora: {} - {}, Alarm: {}".format(str(self.id_data_loger), str(self.temperature), str(self.date), str(self.hours), str(self.alarm))


class Container (models.Model):
    serial = models.CharField(max_length=200)
    destino = models.CharField(max_length=200)
    salida = models.CharField(max_length=200)
    status = models.BooleanField()

    def __str__(self):
        return "Container: {}, Salio de: {}, llega a: {}, Estatus: {}".format(str(self.serial), str(self.salida), str(self.destino), str(self.status))


class Location (models.Model):
    
    countryName = models.CharField(max_length=200)
    countryCode = models.CharField(max_length=200)
    adminArea =  models.CharField(max_length=200)
    locality = models.CharField(max_length=200)
    postalCode = models.IntegerField()
    latitud = models.FloatField()
    longitud = models.FloatField()
    id_data_loger = models.ForeignKey('Data_Loger', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.countryName

class Heredity (models.Model):
    datalogger_father = models.CharField(max_length=200)
    datalogger_son = models.CharField(max_length=200)

    def __str__ (self):
        return "Father: {}, and Son: {}". format(str(self.datalogger_father), str(self.datalogger_son))
