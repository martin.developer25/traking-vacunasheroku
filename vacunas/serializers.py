from rest_framework import serializers
from .models import Data, Data_Loger, Container, Location, Heredity
#from django.contrib.auth.models import User
#from django.contrib.auth import authenticate

# User Serializer

"""
user 
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


# Register Serializaer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'], validated_data['email'], validated_data['password'])

        return user


# Login Serializer
class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Incorrect Credentials")
"""

class DataLogerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data_Loger
        fields = [
            'id_container',
            'id_data_loger',
            'id_status',
        ]
        


class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = [
            'id',
            'id_data_loger',
            'temperature',
            'date',
            'hours',
            'alarm',
            'date_transmition',
            'hours_transmition',
            'created_on',
            'updated_on',
          #  'owner'
        ]
        

class ContainerSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'serial',
            'destino',
            'salida',
            'status',
        )
        model = Container


class LocationSerializer(serializers.ModelSerializer):
    #id_data_loger= DataLogerSerializer()
    class Meta:
        model = Location
        fields = [
            'id',
            'countryName',
            'countryCode',
            'adminArea',
            'locality',
            'postalCode',
            'latitud',
            'longitud',
            'id_data_loger',
        ]
        
    

class HereditySerializer(serializers.ModelSerializer):
    class Meta:
        model = Heredity
        fields = [
            'id',
            'datalogger_father',
            'datalogger_son',
        ]
        