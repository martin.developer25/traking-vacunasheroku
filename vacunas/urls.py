from django.urls import path, include
from . import views
from .views import (
    ListContainer,
    dataViewSet,
    DataLoggerViewSet,
    LocationViewSet,
    HeredityViewSet,
    #RegisterAPI, LoginAPI, UserAPI,
)
from knox import views as knox_views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'listdata', dataViewSet,basename='data')
router.register(r'heredity', HeredityViewSet,basename='heredity')
router.register(r'location', LocationViewSet,basename='location')
router.register(r'datalogger', DataLoggerViewSet,basename='location')
#router.register(r'createdata', dataCreateViewSet)

urlpatterns = [
    path('', include(router.urls)),
    #path('data/', views.ListData.as_view()),
    
    path('auth', include('knox.urls')),
    #path('auth/register', RegisterAPI.as_view()),
    #path('auth/login', LoginAPI.as_view()),
    #path('auth/user', UserAPI.as_view()),
    #path('auth/logout', knox_views.LogoutView.as_view(), name='knox_logout'),
]
