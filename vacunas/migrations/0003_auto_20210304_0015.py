# Generated by Django 3.1.4 on 2021-03-04 00:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vacunas', '0002_auto_20210226_2023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='data_loger',
            name='id_data_loger',
            field=models.CharField(max_length=200, primary_key=True, serialize=False, unique=True),
        ),
        migrations.AlterField(
            model_name='data_loger',
            name='id_status',
            field=models.BooleanField(null=True),
        ),
    ]
