from django.contrib import admin
from .models import Data, Data_Loger, Container, Location, Heredity

# Register your models here.
admin.site.register(Data)
admin.site.register(Data_Loger)
admin.site.register(Container)
admin.site.register(Location)
admin.site.register(Heredity)